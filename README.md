***Auto Config Loader***
This is a library dependency that can be used to build a configuration file that supports a number of key features

1. Dynamic updating
2. Dynamic saving and reloading
3. Consistent Yaml Structure

===Limitations===
Use only primitive types , Sets and Lists as arrays of primitives or thier wrappers.

License 
MIT
