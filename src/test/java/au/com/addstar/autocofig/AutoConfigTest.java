package au.com.addstar.autocofig;

import java.io.File;
import java.util.List;

import org.bukkit.configuration.InvalidConfigurationException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.support.io.TempDirectory.*;


import static org.junit.jupiter.api.Assertions.*;

/**
 * Created for the AddstarMC Project. Created by Narimm on 30/01/2019.
 */
public class AutoConfigTest {
    
    @TempDir
    private static File folder;
    private static File file;
    
    @BeforeAll
    public static void setUp() {
        File cfile =  new File("target");
        if(!cfile.exists())cfile.mkdir();
        folder = new File(cfile,"working");
        folder.mkdir();
        file = new File(folder,"Test.yml");
    }
    
    @AfterEach
    public void after(){
        file.delete();
    }
    
    @Test
    public void getConfigFieldsTest() {
        ConfigurationTest config = new ConfigurationTest(file);
        config.save();
        List<String> fields = config.getConfigFields();
        assertTrue(fields.contains("testBool"));
    }
    
    @Test
    public void updateFieldValue() {
        ConfigurationTest config = new ConfigurationTest(file);
        assertEquals(1,config.testInt);
        assertTrue(config.updateFieldValue("testInt",2));
        assertEquals(2,config.testInt);
        assertFalse(config.updateFieldValue("testInt",0D));
        assertEquals(2,config.testInt);
        assertFalse(config.updateFieldValue("testInt","one"));
        assertEquals(2,config.testInt);
    
    }
    @Test
    public void testSetCategoryComment(){
        ConfigurationTest config = new ConfigurationTest(file);
        config.setCategoryComment("tests","A suite of test values");
        config.setFileHeader("Config fot test Suite","Some other comment");
        config.save();
        
    }
    
    @Test
    public void getFieldType() throws NoSuchFieldException {
        ConfigurationTest config = new ConfigurationTest(file);
        assertSame(boolean.class,config.getFieldType("testBool"));
    }
    
    
    @Test
    public void getFieldValueAsString() throws NoSuchFieldException, IllegalAccessException {
        ConfigurationTest config = new ConfigurationTest(file);
        assertEquals("1",config.getFieldValueAsString("testInt"));
    }
    
    @Test
    public void loadSave() {
        ConfigurationTest config = new ConfigurationTest(file);
        config.setCategoryComment("tests","A suite of test values");
        config.setFileHeader("Config fot test Suite","Some other comment");
        assertTrue(!config.testBool);
        config.testBool = true;
        config.save();
        ConfigurationTest config2 = new ConfigurationTest(file);
        config2.load();
        assertTrue(config2.testBool);
        File file = new File(folder,"test2.yml");
        config2 = new ConfigurationTest(file);
        config2.load();
        config2.save();
        assertFalse(config2.testBool);
    }
    
    @Test
    public void valueSetter() throws IllegalAccessException, NoSuchFieldException, InvalidConfigurationException {
        ConfigurationTest config = new ConfigurationTest(file);
        assertTrue(!config.testBool);
        config.valueSetter("testBool","true");
        assertEquals(0, config.testListInteger.size());
        assertEquals(0, config.testListDouble.size());
        assertEquals(0, config.testListLong.size());
        config.valueSetter("testListInteger","1,2,3");
        config.valueSetter("testSetInteger","1,2,3");
        config.valueSetter("arrayInt","1,2,3");
        config.valueSetter("testListDouble","1,2,3");
        config.valueSetter("testSetDouble","1,2,3");
        config.valueSetter("arrayDbl","1,2,3");
        config.valueSetter("testlistShorts","1,2,3");
        config.valueSetter("testSetShort","1,2,3");
        config.valueSetter("arrayShort","1,2,3");
        config.valueSetter("testListLong","1,2,3");
        config.valueSetter("testSetLong","1,2,3");
        config.valueSetter("arrayLng","1,2,3");
        config.valueSetter("testListFloat","1,2,3");
        config.valueSetter("testSetFloat","1,2,3");
        config.valueSetter("arrayFloat","1,2,3");
        config.valueSetter("testListBoolean","true,false,true");
        config.valueSetter("testSetBoolean","true,false,true");
        config.valueSetter("arrayBool","true,false,true");
        config.valueSetter("testListString","one,two,three");
        config.valueSetter("testSetString","one,two,three");
        config.valueSetter("arrayString","one,two,three");
        config.valueSetter("arrayString","one");
        config.valueSetter("arrayString","");
        config.valueSetter("testStr","");
        config.valueSetter("testStr","test");
        config.valueSetter("testDouble","1");
        config.valueSetter("testShort","0");
        config.valueSetter("testInt","1");
       assertThrows(NoSuchFieldException.class,()->config.valueSetter("testInteger","1"));
        config.valueSetter("privateTestBool","true");
        assertEquals(3, config.testListDouble.size());
        assertEquals(3, config.testListInteger.size());
        assertEquals(3, config.testListLong.size());
        assertTrue(config.testBool);
        config.save();
        ConfigurationTest config2 = new ConfigurationTest(file);
        config2.load();
        assertEquals(3, config2.testListLong.size());
    
    }
}