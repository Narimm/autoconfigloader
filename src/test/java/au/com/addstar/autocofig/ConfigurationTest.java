package au.com.addstar.autocofig;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created for the AddstarMC Project. Created by Narimm on 30/01/2019.
 */
public class ConfigurationTest extends AutoConfig {
    protected ConfigurationTest(File file) {
        super(file);
    }
    @ConfigField(name = "testBoolean",comment = "A Test boolean",category = "tests")
    public boolean testBool = false;
    @ConfigField(name = "privateBool",comment = "A Test boolean",category = "tests")
    private boolean privateTestBool = false;
    @ConfigField(name = "testInteger",comment = "A Test Integer",category = "tests")
    public int testInt = 1;
    @ConfigField(name = "testFloat",comment = "A Test Float",category = "tests")
    public float testFloat = 1F;
    @ConfigField(name = "testDouble",comment = "A Test Double",category = "tests")
    public double testDouble = 1D;
    @ConfigField(name = "testString",comment = "A Test String",category = "tests")
    public String testStr = "A Test String";
    @ConfigField(name = "testShort",comment = "A Test Short",category = "tests")
    public short testShort = 1;
    @ConfigField(name = "testLong",comment = "A Test Long",category = "tests")
    private long testLong = 1L;
    @ConfigField(name = "listString",comment = "A Test List of Strings",category = "tests")
    public ArrayList<String> testListString = new ArrayList<>();
    @ConfigField
    public ArrayList<Short> testlistShorts = new ArrayList<>();
    @ConfigField(name = "floatList",comment = "A Test List of Strings",category = "tests")
    public ArrayList<Float> testListFloat = new ArrayList<>();
    @ConfigField(name = "IntList",comment = "A Test List of Strings",category = "tests")
    public ArrayList<Integer> testListInteger = new ArrayList<>();
    @ConfigField(name = "dblList",comment = "A Test List of Strings",category = "tests")
    public ArrayList<Double> testListDouble = new ArrayList<>();
    @ConfigField(name = "lngList",comment = "A Test List of Strings",category = "tests")
    public ArrayList<Long> testListLong= new ArrayList<>();
    @ConfigField(name = "boolList",comment = "A Test List of Strings",category = "tests")
    public ArrayList<Boolean> testListBoolean = new ArrayList<>();
    @ConfigField
    public String[] arrayString = {};
    @ConfigField
    public float[] arrayFloat = {1F,2F,3F};
    @ConfigField
    public int[] arrayInt = {};
    @ConfigField
    public double[] arrayDbl = {};
    @ConfigField
    public long[] arrayLng = {};
    @ConfigField
    public boolean[] arrayBool = {};
    @ConfigField
    public short[] arrayShort = {};
    @ConfigField
    public HashSet<String> testSetString = new HashSet<>();
    @ConfigField
    public HashSet<Float> testSetFloat = new HashSet<>();
    @ConfigField
    public HashSet<Integer> testSetInteger = new HashSet<>();
    @ConfigField
    public HashSet<Short> testSetShort = new HashSet<>();
    @ConfigField
    public HashSet<Double> testSetDouble = new HashSet<>();
    @ConfigField
    public HashSet<Long> testSetLong= new HashSet<>();
    @ConfigField
    public HashSet<Boolean> testSetBoolean = new HashSet<>();
}
