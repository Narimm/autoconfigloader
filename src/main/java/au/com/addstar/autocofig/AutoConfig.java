package au.com.addstar.autocofig;

/**
 * Created for the AddstarMC Project. Created by Narimm on 30/01/2019.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.*;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.YamlConfigurationOptions;
import org.bukkit.inventory.ItemStack;

/**
 * Created for the AddstarMC Project. Created by Narimm on 23/01/2019.
 */

public abstract class AutoConfig {
    private File mFile;
    private HashMap<String, String> mCategoryComments;
    private List<String> fileHeader;
    
    protected AutoConfig(File file) {
        mFile = file;
        mCategoryComments = new HashMap<>();
        fileHeader = new ArrayList<>();
    }
    
    protected void setCategoryComment(String category, String comment) {
        mCategoryComments.put(category, comment);
    }
    
    public List<String> getConfigFields() {
        List<String> result = new ArrayList<>();
        try {
            for (Field field : getClass().getDeclaredFields()) {
                result.add(field.getName());
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }
        return result;
    }
    
    public boolean setFileHeader(String... args){
        return fileHeader.addAll(Arrays.asList(args));
    }
    
    public boolean updateFieldValue(String fieldName, Object value) {
        try {
            final Field field = getClass().getField(fieldName);
            field.setAccessible(true);
            field.set(this, value);
            return true;
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            return false;
        }
        
    }
    
    public Class getFieldType(String fieldName) throws NoSuchFieldException {
        final Field field = getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.getType();
    }
    
    
    public String getFieldValueAsString(String fieldName) throws NoSuchFieldException,IllegalAccessException {
        final Field field = getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        Object value = field.get(this);
        return value.toString();
    }
    
    @SuppressWarnings("unchecked")
    public boolean load() {
        YamlConfiguration yml = new YamlConfiguration();
        try {
            // Make sure the file exists
            if (!mFile.exists()) {
                mFile.getParentFile().mkdirs();
                mFile.createNewFile();
            }
            
            // Parse the config
            yml.load(mFile);
            YamlConfigurationOptions opts = yml.options();
            String header = opts.header();
            if(header != null) {
                String[] split = header.split("\n");
                List<String> out = Arrays.asList(split);
                for (String line : out) {
                    if (line.length() == 0) {
                        break;
                    }
                    fileHeader.add(line);
                }
            }
            for (Field field : getClass().getDeclaredFields()) {
                ConfigField configField = field.getAnnotation(ConfigField.class);
                if (configField == null)
                    continue;
                
                String optionName = configField.name();
                if (optionName.isEmpty())
                    optionName = field.getName();
                
                field.setAccessible(true);
                
                String path = (configField.category().isEmpty() ? "" : configField.category() + ".") + optionName;   //$NON-NLS-2$
                if (!yml.contains(path)) {
                    if (field.get(this) == null)
                        throw new InvalidConfigurationException(path + " is required to be set! Info:\n" + configField.comment());
                } else {
                    // Parse the value
                    
                    if (field.getType().isArray()) {
                        // Integer
                        if (field.getType().getComponentType().equals(Integer.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getIntegerList(path).toArray(new Integer[0])));
                            // Float
                        else if (field.getType().getComponentType().equals(Float.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getFloatList(path).toArray(new Float[0])));
                            // Double
                        else if (field.getType().getComponentType().equals(Double.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getDoubleList(path).toArray(new Double[0])));
                        
                            // Long
                        else if (field.getType().getComponentType().equals(Long.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getLongList(path).toArray(new Long[0])));
                            
                            // Short
                        else if (field.getType().getComponentType().equals(Short.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getShortList(path).toArray(new Short[0])));
                            
                            // Boolean
                        else if (field.getType().getComponentType().equals(Boolean.TYPE))
                            field.set(this, ArrayUtils.toPrimitive(yml.getBooleanList(path).toArray(new Boolean[0])));
                            
                            // String
                        else if (field.getType().getComponentType().equals(String.class)) {
                            field.set(this, yml.getStringList(path).toArray(new String[0]));
                        } else
                            throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
                    } else if (List.class.isAssignableFrom(field.getType())) {
                        if (field.getGenericType() == null)
                            throw new IllegalArgumentException("Cannot use type List without specifying generic type for AutoConfiguration");
                        
                        Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                        
                        if (type.equals(Integer.class))
                            field.set(this, newList((Class<? extends List<Integer>>) field.getType(), yml.getIntegerList(path)));
                        else if (type.equals(Float.class))
                            field.set(this, newList((Class<? extends List<Float>>) field.getType(), yml.getFloatList(path)));
                        else if (type.equals(Double.class))
                            field.set(this, newList((Class<? extends List<Double>>) field.getType(), yml.getDoubleList(path)));
                        else if (type.equals(Long.class))
                            field.set(this, newList((Class<? extends List<Long>>) field.getType(), yml.getLongList(path)));
                        else if (type.equals(Short.class))
                            field.set(this, newList((Class<? extends List<Short>>) field.getType(), yml.getShortList(path)));
                        else if (type.equals(Boolean.class))
                            field.set(this, newList((Class<? extends List<Boolean>>) field.getType(), yml.getBooleanList(path)));
                        else if (type.equals(String.class))
                            field.set(this, newList((Class<? extends List<String>>) field.getType(), yml.getStringList(path)));
                        else
                            throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-2$
                    } else if (Set.class.isAssignableFrom(field.getType())) {
                        if (field.getGenericType() == null)
                            throw new IllegalArgumentException("Cannot use type set without specifying generic type for AytoConfiguration");
                        
                        Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                        if (type.equals(Integer.class))
                            field.set(this, newSet((Class<? extends Set<Integer>>) field.getType(), yml.getIntegerList(path)));
                        else if (type.equals(Float.class))
                            field.set(this, newSet((Class<? extends Set<Float>>) field.getType(), yml.getFloatList(path)));
                        else if (type.equals(Double.class))
                            field.set(this, newSet((Class<? extends Set<Double>>) field.getType(), yml.getDoubleList(path)));
                        else if (type.equals(Long.class))
                            field.set(this, newSet((Class<? extends Set<Long>>) field.getType(), yml.getLongList(path)));
                        else if (type.equals(Short.class))
                            field.set(this, newSet((Class<? extends Set<Short>>) field.getType(), yml.getShortList(path)));
                        else if (type.equals(Boolean.class))
                            field.set(this, newSet((Class<? extends Set<Boolean>>) field.getType(), yml.getBooleanList(path)));
                        else if (type.equals(String.class))
                            field.set(this, newSet((Class<? extends Set<String>>) field.getType(), yml.getStringList(path)));
                        else
                            throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-2$
                    } else {
                        // Integer
                        if (field.getType().equals(Integer.TYPE))
                            field.setInt(this, yml.getInt(path));
                            
                            // Float
                        else if (field.getType().equals(Float.TYPE))
                            field.setFloat(this, (float) yml.getDouble(path));
                            
                            // Double
                        else if (field.getType().equals(Double.TYPE))
                            field.setDouble(this, yml.getDouble(path));
                            
                            // Long
                        else if (field.getType().equals(Long.TYPE))
                            field.setLong(this, yml.getLong(path));
                            
                            // Short
                        else if (field.getType().equals(Short.TYPE))
                            field.setShort(this, (short) yml.getInt(path));
                            
                            // Boolean
                        else if (field.getType().equals(Boolean.TYPE))
                            field.setBoolean(this, yml.getBoolean(path));
                            
                            // ItemStack
                        else if (field.getType().equals(ItemStack.class))
                            field.set(this, yml.getItemStack(path));
                            
                            // String
                        else if (field.getType().equals(String.class))
                            field.set(this, yml.getString(path));
                        else
                            throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
                    }
                }
            }
            
            onPostLoad();
            
            return true;
        } catch (IOException | IllegalAccessException | IllegalArgumentException | InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    private <T> List<T> newList(Class<? extends List<T>> listClass, Collection<T> data) throws InvalidConfigurationException {
        Validate.isTrue(!Modifier.isAbstract(listClass.getModifiers()), "You cannot use an abstract type for AutoConfiguration");
        
        Constructor<? extends List<T>> constructor;
        
        try {
            constructor = listClass.getConstructor(Collection.class);
            
            return constructor.newInstance(data);
        } catch (Exception e) {
            throw new InvalidConfigurationException(e);
        }
    }
    
    private <T> Set<T> newSet(Class<? extends Set<T>> setClass, Collection<T> data) throws InvalidConfigurationException {
        Validate.isTrue(!Modifier.isAbstract(setClass.getModifiers()), "You cannot use an abstract type for AutoConfiguration");
        
        Constructor<? extends Set<T>> constructor;
        
        try {
            constructor = setClass.getConstructor(Collection.class);
            
            return constructor.newInstance(data);
        } catch (Exception e) {
            throw new InvalidConfigurationException(e);
        }
    }
    
    /**
     * This should be used to process any data loaded from the config including performing validation and translation
     *
     * @throws InvalidConfigurationException Thrown if a field has an invalid value
     */
    protected void onPostLoad() throws InvalidConfigurationException {}
    
    public boolean save() {
        try {
            onPreSave();
            
            YamlConfiguration config = new YamlConfiguration();
            
            // Add all the category comments
            Map<String, String> comments = new HashMap<>(mCategoryComments);
            
            // Add all the values
            for (Field field : getClass().getDeclaredFields()) {
                ConfigField configField = field.getAnnotation(ConfigField.class);
                if (configField == null)
                    continue;
                String optionName = configField.name();
                if (optionName.isEmpty())
                    optionName = field.getName();
                
                field.setAccessible(true);
                StringBuilder header = new StringBuilder();
                for(String s:fileHeader){
                    header.append(s).append("\n");
                }
                config.options().header(header.toString());
                String path = (configField.category().isEmpty() ? "" : configField.category() + ".") + optionName;   //$NON-NLS-2$
                // Ensure the secion exists
                if (!configField.category().isEmpty() && !config.contains(configField.category()))
                    config.createSection(configField.category());
                
                if (field.getType().isArray()) {
                    // Integer
                    if (field.getType().getComponentType().equals(Integer.TYPE))
                        config.set(path, Arrays.asList((int[]) field.get(this)));
                        
                        // Float
                    else if (field.getType().getComponentType().equals(Float.TYPE)) {
                        Object o = field.get(this);
                        config.set(path, Arrays.asList((float[]) o));
                    }
                        // Double
                    else if (field.getType().getComponentType().equals(Double.TYPE))
                        config.set(path, Arrays.asList((double[]) field.get(this)));
                        
                        // Long
                    else if (field.getType().getComponentType().equals(Long.TYPE))
                        config.set(path, Arrays.asList((long[]) field.get(this)));
                        
                        // Short
                    else if (field.getType().getComponentType().equals(Short.TYPE))
                        config.set(path, Arrays.asList((short[]) field.get(this)));
                        
                        // Boolean
                    else if (field.getType().getComponentType().equals(Boolean.TYPE))
                        config.set(path, Arrays.asList((boolean[]) field.get(this)));
                        
                        // String
                    else if (field.getType().getComponentType().equals(String.class))
                        config.set(path, Arrays.asList((String[]) field.get(this)));
                    else
                        throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
                } else if (List.class.isAssignableFrom(field.getType())) {
                    if (field.getGenericType() == null)
                        throw new IllegalArgumentException("Cannot use type List without specifying generic type for AutoConfiguration");
                    
                    Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                    
                    if (type.equals(Integer.class) || type.equals(Float.class)
                            || type.equals(Double.class) || type.equals(Long.class)
                            || type.equals(Short.class) || type.equals(Boolean.class)
                            || type.equals(String.class)) {
                        config.set(path, field.get(this));
                    } else
                        throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-3$
                } else if (Set.class.isAssignableFrom(field.getType())) {
                    if (field.getGenericType() == null)
                        throw new IllegalArgumentException("Cannot use type Set without specifying generic type for AutoConfiguration");
                    
                    Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                    
                    if (type.equals(Integer.class) || type.equals(Float.class)
                            || type.equals(Double.class) || type.equals(Long.class)
                            || type.equals(Short.class) || type.equals(Boolean.class)
                            || type.equals(String.class)) {
                        config.set(path, new ArrayList<Object>((Set<?>) field.get(this)));
                    } else
                        throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-3$
                } else {
                    // Integer
                    if (field.getType().equals(Integer.TYPE))
                        config.set(path, field.get(this));
                        
                        // Float
                    else if (field.getType().equals(Float.TYPE))
                        config.set(path, field.get(this));
                        
                        // Double
                    else if (field.getType().equals(Double.TYPE))
                        config.set(path, field.get(this));
                        
                        // Long
                    else if (field.getType().equals(Long.TYPE))
                        config.set(path, field.get(this));
                        
                        // Short
                    else if (field.getType().equals(Short.TYPE))
                        config.set(path, field.get(this));
                        
                        // Boolean
                    else if (field.getType().equals(Boolean.TYPE))
                        config.set(path, field.get(this));
                        
                        // ItemStack
                    else if (field.getType().equals(ItemStack.class))
                        config.set(path, field.get(this));
                        
                        // String
                    else if (field.getType().equals(String.class))
                        config.set(path, field.get(this));
                    else
                        throw new IllegalArgumentException("Field: "+ field.getName() + " Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
                }
                
                // Record the comment
                if (!configField.comment().isEmpty())
                    comments.put(path, configField.comment());
            }
            
            StringBuilder output = new StringBuilder(config.saveToString());
            
            // Apply comments
            String category = "";
            List<String> lines = new ArrayList<>(Arrays.asList(output.toString().split("\n")));
            for (int l = 0; l < lines.size(); l++) {
                String line = lines.get(l);
                
                if (line.startsWith("#"))
                    continue;
                
                if (line.trim().startsWith("-"))
                    continue;
                
                if (!line.contains(":"))
                    continue;
                
                String path;
                line = line.substring(0, line.indexOf(":"));
                
                if (line.startsWith("  "))
                    path = category + "." + line.substring(2).trim();
                else {
                    category = line.trim();
                    path = line.trim();
                }
                
                if (comments.containsKey(path)) {
                    String indent = "";
                    for (int i = 0; i < line.length(); i++) {
                        if (line.charAt(i) == ' ')
                            indent += " ";
                        else
                            break;
                    }
                    
                    // Add in the comment lines
                    String[] commentLines = comments.get(path).split("\n");
                    lines.add(l++, "");
                    for (int i = 0; i < commentLines.length; i++) {
                        commentLines[i] = indent + "# " + commentLines[i];
                        lines.add(l++, commentLines[i]);
                    }
                }
            }
            output = new StringBuilder();
            for (String line : lines) {
                output.append(line).append("\n");
            }
            
            FileWriter writer = new FileWriter(mFile);
            writer.write(output.toString());
            writer.close();
            return true;
        } catch (IllegalArgumentException | IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    /**
     * This should be used to update/convert any data in fields for saving
     */
    protected void onPreSave() {}
    
    private List<String> getListfromString(String value) {
        String[] out = value.split(",");
        if(out.length == 0) {
            String[] arr = {value};
            return  Arrays.asList(arr);
        }
        return Arrays.asList(out);
    }
    
    @SuppressWarnings("unchecked")
    public void valueSetter(String fieldname, String value) throws IllegalAccessException, InvalidConfigurationException, NoSuchFieldException {
        Field field = getClass().getDeclaredField(fieldname);
        field.setAccessible(true);
        if (field.getType().isArray()) {
            List<String> valueList = getListfromString(value);
            // Integer
            if (field.getType().getComponentType().equals(Integer.TYPE)) {
                List<Integer> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Integer.valueOf(s));
                }
                field.set(this, ArrayUtils.toPrimitive(in.toArray(new Integer[0])));
            }
            // Float
            else if (field.getType().getComponentType().equals(Float.TYPE)) {
                List<Float> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Float.valueOf(s));
                }
                field.set(this, ArrayUtils.toPrimitive(in.toArray(new Float[0])));
                
            }// Double
            else if (field.getType().getComponentType().equals(Double.TYPE)) {
                List<Double> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Double.valueOf(s));
                }
                field.set(this, ArrayUtils.toPrimitive(in.toArray(new Double[0])));
            }
            // Long
            else if (field.getType().getComponentType().equals(Long.TYPE)) {
                List<Long> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Long.valueOf(s));
                }
                field.set(this, ArrayUtils.toPrimitive(in.toArray(new Long[0])));
            }
            // Short
            else if (field.getType().getComponentType().equals(Short.TYPE)) {
                List<Short> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Short.valueOf(s));
                }
                field.set(this, ArrayUtils.toPrimitive(in.toArray(new Short[0])));
            }
            // Boolean
            else if (field.getType().getComponentType().equals(Boolean.TYPE)) {
                List<Boolean> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Boolean.valueOf(s));
                }
                field.set(this,ArrayUtils.toPrimitive(in.toArray(new Boolean[0])));
            }
            // String
            else if (field.getType().getComponentType().equals(String.class)) {
                
                field.set(this, valueList.toArray(new String[0]));
            } else
                throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
        } else if (List.class.isAssignableFrom(field.getType())) {
            List<String> valueList = getListfromString(value);
            if (field.getGenericType() == null)
                throw new IllegalArgumentException("Cannot use type List without specifying generic type for AutoConfiguration");
            
            Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            
            if (type.equals(Integer.class)) {
                List<Integer> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Integer.valueOf(s));
                }
                
                field.set(this, newList((Class<? extends List<Integer>>) field.getType(), in));
            } else if (type.equals(Float.class)) {
                List<Float> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Float.valueOf(s));
                }
                field.set(this, newList((Class<? extends List<Float>>) field.getType(), in));
            } else if (type.equals(Double.class)) {
                List<Double> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Double.valueOf(s));
                }
                field.set(this, newList((Class<? extends List<Double>>) field.getType(), in));
            } else if (type.equals(Long.class)) {
                List<Long> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Long.valueOf(s));
                }
                field.set(this, newList((Class<? extends List<Long>>) field.getType(), in));
            } else if (type.equals(Short.class)) {
                List<Short> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Short.valueOf(s));
                }
                field.set(this, newList((Class<? extends List<Short>>) field.getType(), in));
            } else if (type.equals(Boolean.class)) {
                List<Boolean> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Boolean.valueOf(s));
                }
                field.set(this, newList((Class<? extends List<Boolean>>) field.getType(), in));
            } else if (type.equals(String.class))
                field.set(this, newList((Class<? extends List<String>>) field.getType(), valueList));
            else
                throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-2$
        } else if (Set.class.isAssignableFrom(field.getType())) {
            List<String> valueList = getListfromString(value);
            if (field.getGenericType() == null)
                throw new IllegalArgumentException("Cannot use type set without specifying generic type for AytoConfiguration");
            
            Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            if (type.equals(Integer.class)) {
                List<Integer> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Integer.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Integer>>) field.getType(), in));
            } else if (type.equals(Float.class)) {
                List<Float> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Float.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Float>>) field.getType(), in));
            } else if (type.equals(Double.class)) {
                List<Double> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Double.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Double>>) field.getType(), in));
            } else if (type.equals(Long.class)) {
                List<Long> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Long.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Long>>) field.getType(), in));
            } else if (type.equals(Short.class)) {
                List<Short> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Short.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Short>>) field.getType(), in));
            } else if (type.equals(Boolean.class)) {
                List<Boolean> in = new ArrayList<>();
                for (String s : valueList) {
                    in.add(Boolean.valueOf(s));
                }
                field.set(this, newSet((Class<? extends Set<Boolean>>) field.getType(), in));
            } else if (type.equals(String.class))
                field.set(this, newSet((Class<? extends Set<String>>) field.getType(), valueList));
            else
                throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + "<" + type.toString() + "> for AutoConfiguration");   //$NON-NLS-2$ //$NON-NLS-2$
        } else {
            // Integer
            if (field.getType().equals(Integer.TYPE))
                field.setInt(this, Integer.valueOf(value));
                
                // Float
            else if (field.getType().equals(Float.TYPE))
                field.setFloat(this, Float.valueOf(value));
                
                // Double
            else if (field.getType().equals(Double.TYPE))
                field.setDouble(this, Double.valueOf(value));
                
                // Long
            else if (field.getType().equals(Long.TYPE))
                field.setLong(this, Long.valueOf(value));
                
                // Short
            else if (field.getType().equals(Short.TYPE))
                field.setShort(this, Short.valueOf(value));
                
                // Boolean
            else if (field.getType().equals(Boolean.TYPE))
                field.setBoolean(this, Boolean.valueOf(value));
                
                // ItemStack
            else if (field.getType().equals(ItemStack.class))
                throw new IllegalArgumentException("Cannot use update type : " + field.getType().getSimpleName() + " via commands");
                
                // String
            else if (field.getType().equals(String.class))
                field.set(this, value);
            else
                throw new IllegalArgumentException("Cannot use type " + field.getType().getSimpleName() + " for AutoConfiguration");   //$NON-NLS-2$
        }
    }
}
