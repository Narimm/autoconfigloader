package au.com.addstar.autocofig;

/**
 * Created for the AddstarMC Project. Created by Narimm on 30/01/2019.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigField {
    String name() default "";
    
    String comment() default "";
    
    String category() default "";
    
}